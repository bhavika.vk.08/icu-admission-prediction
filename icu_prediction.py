import numpy as np
import pandas as pd
import os
import plotly.express as px
import matplotlib.pyplot as plt
from plotly.subplots import make_subplots
import seaborn as sns
import plotly.graph_objects as go

data = pd.read_excel('/content/Kaggle_Sirio_Libanes_ICU_Prediction.xlsx')
data.head()

print(data.shape)
print("This dataset has ", data.shape[0], " rows and ", data.shape[1], " columns.")

print(data.columns)

data.dtypes

data['ICU'].value_counts()

sns.countplot(x="ICU", data=data)

data.describe()

#age above 65
ax = sns.countplot(x="AGE_ABOVE65", hue="ICU", data=data)

# GENDER
ax = sns.countplot(x="GENDER", hue="ICU", data=data)

sns.countplot(x="IMMUNOCOMPROMISED", hue="ICU", data=data)

sns.countplot(x="OTHER", hue="ICU", data=data)

#hyper tension
sns.countplot(x="HTN", hue="ICU", data=data)

disease_grp = ['DISEASE GROUPING 1','DISEASE GROUPING 2','DISEASE GROUPING 3',
               'DISEASE GROUPING 4', 'DISEASE GROUPING 5', 'DISEASE GROUPING 6']

fig, axes = plt.subplots(ncols=2,nrows=3,figsize=(15,10))

col = 0

for i in range(len(disease_grp)):
    sns.countplot(x=disease_grp[i], hue="ICU", data=data, ax = axes[int((i)/2)][col])
    col = (col+1) % 2

vital_signs = data.columns[13:]
vital_signs_df = data[vital_signs]
f,ax = plt.subplots(figsize=(23,23))
sns.heatmap(vital_signs_df.drop(columns=['ICU']).corr())
plt.show()

data.dtypes

data.isnull().sum()

sns.heatmap(data.isnull())

data['PATIENT_VISIT_IDENTIFIER'].value_counts()

len(data[data["ICU"] == 1]['PATIENT_VISIT_IDENTIFIER'].unique())

general_cols = ['PATIENT_VISIT_IDENTIFIER','AGE_ABOVE65','AGE_PERCENTIL','GENDER','IMMUNOCOMPROMISED']
icu_set = data[data['ICU'] == 1][general_cols].drop_duplicates(keep = 'first')
icu_set

icu_patients = icu_set['PATIENT_VISIT_IDENTIFIER'].unique()

model_data = data[data['ICU'] == 0]
model_data.head()

model_data['ICU_Admitted'] = np.where(model_data['PATIENT_VISIT_IDENTIFIER'].isin(icu_patients), 1 , 0)

model_data[model_data["ICU_Admitted"] == 1]

model_data.drop('ICU', axis = 'columns', inplace = True)
model_data

const_columns = list(model_data.columns[model_data.nunique() <= 1])
const_columns

model_data.drop(const_columns, axis = 'columns', inplace = True)
model_data

model_data = model_data[model_data['PATIENT_VISIT_IDENTIFIER'] != 199]

print(model_data.isna().sum()[model_data.isna().sum() > 0])

model_data = model_data.sort_values(by=['PATIENT_VISIT_IDENTIFIER', 'WINDOW'])\
    .groupby('PATIENT_VISIT_IDENTIFIER', as_index=False)\
    .fillna(method='ffill')\
    .fillna(method='bfill')

model_data.isna().sum().sum()

sns.heatmap(model_data.isnull())

model_data.dtypes

model_data['AGE_PERCENTIL'] = model_data['AGE_PERCENTIL'].astype("category")
model_data['WINDOW'] = model_data['WINDOW'].astype("category")

#Create windows dummies dataframe
window_dummy = pd.get_dummies(model_data['WINDOW'])
#concat this to main dataframe
model_data = pd.concat([model_data, window_dummy], axis =1)
# Remove the WINDOW column 
model_data = model_data.drop(columns=['WINDOW'])
#create AGE_PERCENTIL dummy dataframe
AGE_PERCENTIL_dummy = pd.get_dummies(model_data['AGE_PERCENTIL'])
#concat this to main dataframe
model_data = pd.concat([model_data, AGE_PERCENTIL_dummy], axis =1)
# Remove the AGE_PERCENTIL column 
model_data = model_data.drop(columns=['AGE_PERCENTIL'])
model_data

def outliers(dff,ft):
  Q1 =dff[ft].quantile(.01)
  Q3 =dff[ft].quantile(.99)
  IQR = Q3-Q1
  lower = Q1-1.5*IQR
  upper = Q3+1.5*IQR

  ls = dff.index[(dff[ft]<lower) | (dff[ft]>upper)]
  return ls

index_list =[]
feature_list = list(model_data.columns)

for f in feature_list:
  index_list.extend(outliers(model_data,f))

index_list= list((dict.fromkeys(index_list)))
print(index_list)

def remove(df,ls):
  ls = sorted(set(ls))
  df = df.drop(ls,axis =0)
  return df

model_data = remove(model_data,index_list)
model_data.shape

from sklearn.model_selection import train_test_split

#Independent Vector
#All columns Except the 'ICU_Admitted' get an array of this
X = model_data.drop('ICU_Admitted', axis = 'columns', inplace = False).values
#Dependent Vector Target column values as an array
y = model_data['ICU_Admitted'].values
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size = 0.33, stratify=y)

from sklearn.preprocessing import StandardScaler

scaler = StandardScaler()
X_train = scaler.fit_transform(np.nan_to_num(X_train))
X_test = scaler.transform(np.nan_to_num(X_test))

from sklearn.linear_model import LogisticRegression
from sklearn import metrics
from sklearn.metrics import precision_recall_fscore_support


logregression=LogisticRegression(max_iter=1000)
logregression.fit(X_train,y_train)
y_prediction=logregression.predict(X_test)

accuracy =  metrics.accuracy_score(y_test, y_prediction)
print('Accuracy=',(accuracy)*100)

from sklearn.neighbors import KNeighborsClassifier
from sklearn import metrics
from sklearn.model_selection import cross_val_score


knn = KNeighborsClassifier(n_neighbors=5, metric='euclidean')
knn.fit(X_train, y_train)
y_pred = knn.predict(X_test)

print("Accuracy:",metrics.accuracy_score(y_test, y_pred)*100)


knn = KNeighborsClassifier(n_neighbors=7, metric='euclidean')
knn.fit(X_train, y_train)
y_pred = knn.predict(X_test)

print("Accuracy:",metrics.accuracy_score(y_test, y_pred)*100)

from xgboost import XGBClassifier
from sklearn.metrics import accuracy_score

model = XGBClassifier()
model.fit(X_train, y_train)

y_pred = model.predict(X_test)
predictions = [round(value) for value in y_pred]

accuracy = accuracy_score(y_test, predictions)
print('Accuracy=',(accuracy)*100)

from sklearn import metrics
from sklearn.ensemble import RandomForestClassifier
from sklearn.metrics import roc_auc_score
from sklearn.metrics import precision_recall_fscore_support

model = RandomForestClassifier(n_jobs=64,n_estimators=200,criterion='entropy',oob_score=True)
model.fit(X_train, y_train)
y_pred = model.predict(X_test)

acc =  metrics.accuracy_score(y_test, y_pred)
print('Accuracy=',(acc)*100)

algo = ['Logistic Regression','KNN','XGB Classifier','Random Forest']
algo_acc = [81.98,82.43,93.02,97.74]
fig, ax = plt.subplots(figsize =(16, 9))
plt.rcParams['font.size'] = '16'
# Horizontal Bar Plot
ax.barh(algo,algo_acc)

# Remove axes splines
for s in ['top', 'bottom', 'left', 'right']:
    ax.spines[s].set_visible(False)
 
# Remove x, y Ticks
ax.xaxis.set_ticks_position('none')
ax.yaxis.set_ticks_position('none')
 
# Add padding between axes and labels
ax.xaxis.set_tick_params(pad = 5)
ax.yaxis.set_tick_params(pad = 10,)
 
# Add x, y gridlines
ax.grid(b = True, color ='grey',
        linestyle ='-.', linewidth = 0.5,
        alpha = 0.2)
 
# Show top values
ax.invert_yaxis()
 
# Add annotation to bars
for i in ax.patches:
    plt.text(i.get_width()+0.2, i.get_y()+0.5,
             str(round((i.get_width()), 2)),
             fontsize = 12,
             color ='black')
 
# Add Plot Title

ax.set_title('Conclusion',
             loc ='left', fontsize=30)
ax.set_ylabel('Algorithm', fontsize=16)
ax.set_xlabel('Accuracy', fontsize=16)
 
plt.show()

